<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Assortments extends CI_Controller
{
    function index()
    {
        $this->load->model('assortments_model');

        $data['assortments'] = $this->assortments_model->get_all_records();


        $this->load->view('assortments_view', $data);
    }

    function edit($id)
    {
        $this->load->model('assortments_model');

        //$this->assortments_model->edit($data);
    }
}