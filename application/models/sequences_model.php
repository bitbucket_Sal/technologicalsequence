<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sequences_model extends CI_Controller
{
    public function add_operation_to_sequence($data)
    {
        return $this->db->insert('specialties', $data);
    }

    public function get_sequence_by_id($model_id)
    {
        $this->db->select('*');
        $this->db->from('sequences');
        $this->db->join('operations', 'operations.id = sequences.operation');
        $result = $this->db->get();

        return $result->result_array();
    }

    public function delete_operation_from_sequence($model_id, $operation_id)
    {
        $this->db->where('model', $model_id);
        $this->db->where('operation', $operation_id);
        return $this->db->delete('sequences');
    }

    public function delete_sequence($model_id)
    {
        $this->db->where('model', $model_id);
        return $this->db->delete('sequences');
    }

    public function get_models_image($model_id)
    {
        $this->db->select('image');
        $this->db->where('id', $model_id);
        return $this->db->get('models');
    }
}