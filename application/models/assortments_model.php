<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Assortments_model extends CI_Model
{
    public function add_record($data)
    {
        return $this->db->insert('assortments', $data);
    }

    public function get_all_records()
    {
        //$this->db->limit(3);
        //$this->db->where('id', '1');
        $this->db->order_by('id', 'DESC');
        $result = $this->db->get('assortments');
        return $result->result_array();
    }

    public function edit_record($data)
    {
        $this->db->where('id', $data['id']);
        return $this->db->update('assortments', $data);
    }

    public function delete_record($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('assortments', $id);
    }

    public function get_record_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('assortments', $id);
    }
}