<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Specialties_model extends CI_Model
{
    public function add_record($data)
    {
        return $this->db->insert('specialties', $data);
    }

    public function get_record_by_id($id)
    {
        $this->db->where('id', $id);
        return  $this->db->get('specialties');
    }

    public function get_all_records()
    {
        $this->db->order_by('id', 'DESC');
        $result = $this->db->get('specialties');
        return $result->result_array();
    }

    public function edit_record($data)
    {
        $this->db->where('id', $data['id']);
        return $this->db->update('specialties', $data);
    }

    public function delete_record($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('specialties');
    }
}