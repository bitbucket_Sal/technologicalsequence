<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Models_model extends CI_Model
{
    public function add_record($data)
    {
        return $this->db->insert('models', $data);
    }

    public function get_record_by_id($id)
    {
        $this->db->where('id', $id);
        return  $this->db->get('models');
    }

    public function get_all_records()
    {
        $this->db->order_by('id', 'DESC');
        $result = $this->db->get('models');
        return $result->result_array();
    }

    public function edit_record($data)
    {
        $this->db->where('id', $data['id']);
        return $this->db->update('models', $data);
    }

    public function delete_record($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('models');
    }
}