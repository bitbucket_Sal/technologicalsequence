<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_model
{
    public function register($data)
    {
        return $this->db->insert('users', $data);
    }

    public function edit($data)
    {
        $this->db->where('id', $data['id']);
        return $this->db->update('users', $data);
    }

    public function check_user_data($username, $password)
    {
        $user = $this->db
            ->select('*')
            ->where('username', $username)
            ->where('password', $password)
            ->get('users')->row_array();

    }
}