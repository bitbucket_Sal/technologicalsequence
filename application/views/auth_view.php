<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Saliev A.A.">
    <title>Авторизация</title>
    
    <!--Подключаемые стили-->
    <link href="/styles/css/bootstrap.min.css" rel="stylesheet">
    <link href="/styles/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="/styles/css/font-awesome.min.css" rel="stylesheet">
    <link href="/styles/css/styles.css" rel="stylesheet">
    
    <!--Подключаемые скрипты-->
    <script src="/styles/js/jquery-1.12.3.min.js"></script>
    <script src="/styles/js/bootstrap.min.js"></script>
    <script src="/styles/js/common/auth.js"></script>
</head>

<body>
    <div class="container">
        <h2 class="title">Система учета технологической последовательности</h2>
        
      <form method="post" action="/login" class="form-signin" id="auth_form" />
        <input type="text" id="username" class="form-control" placeholder="Имя пользователя" required autofocus>
        <input type="password" id="password" class="form-control" placeholder="Пароль" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Вход</button>
        <div id="auth_error" class="alert alert-danger fade">
            <button type="button" class="close">&times;</button><b>.</b>
        </div>
      </form>

    </div>
</body>

</html>