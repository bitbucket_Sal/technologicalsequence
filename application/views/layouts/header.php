<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title><?= $title; ?></title>

    <link href="/styles/css/bootstrap.min.css" rel="stylesheet">
    <link href="/styles/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="/styles/css/font-awesome.css" rel="stylesheet">
    <link href="/styles/css/styles.css" rel="stylesheet">
</head>
</html>
<body>

<div class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a href="/" class="navbar-brand">Главная</a>
        </div>
        <div class="collapse navbar-collapse" id="responsive-menu">
            <?php if (!users_model::is_guest()): ?>
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown">Категории<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="sequences/index" class="list-group-item">Последовательность</a></li>
                            <li><a href="models/index" class="list-group-item">Модель</a></li>
                            <li><a href="assortments/index" class="list-group-item">Ассортимент</a></li>
                            <li><a href="operations/index" class="list-group-item">Операции</a></li>
                            <li><a href="equipments/index" class="list-group-item">Оборудование</a></li>
                            <li><a href="specialties/index" class="list-group-item">Специальность</a></li>
                        </ul>
                    </li>
                </ul>
                <form action="" class="navbar-form navbar-right">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-unlock-alt"> Выход</i>
                    </button>
                </form>
            <?php else: ?>
                <form action="" class="navbar-form navbar-right">
                    <div class="form-group">
                        <input text="text" name="username" class="form-control" placeholder="Логин" value="">
                    </div>
                    <div class="form-group">
                        <input text="password" name="password" class="form-control" placeholder="Пароль" value="">
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary">
                        <i class="fa fa-sign-in"> Вход</i>
                    </button>
                </form>
            <?php endif; ?>
        </div>
    </div>
</div>